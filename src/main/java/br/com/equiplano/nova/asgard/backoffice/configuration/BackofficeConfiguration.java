package br.com.equiplano.nova.asgard.backoffice.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("br.com.equiplano.nova.asgard.backoffice")
@EnableJpaRepositories("br.com.equiplano.nova.asgard.backoffice")
@EnableAsync
@EnableTransactionManagement
public class BackofficeConfiguration {
}
