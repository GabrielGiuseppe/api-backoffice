package br.com.equiplano.nova.asgard.backoffice.model.common;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

import static br.com.equiplano.nova.asgard.backoffice.util.TimeZones.*;

@Getter
@Setter
@MappedSuperclass
public class BaseModel<T extends BaseModel<T>> implements Serializable {

    private static final long serialVersionUID = -1112571230237032841L;

    public static final String ID = "id";
    public static final String DATE_MODEL = "dateModel";

    @Id
    @GeneratedValue
    public Long id;

    @Embedded
    public DateModel dateModel;

    @Version
    public Integer version;


    @PrePersist
    public void initializeDates() {
        LocalDateTime now = LocalDateTime.now(getUtc());
        dateModel = new DateModel(now, now);
    }

    @PreUpdate
    public void updateUpdatedAt() {
        dateModel.updatedAt = LocalDateTime.now(getUtc());
    }
}
