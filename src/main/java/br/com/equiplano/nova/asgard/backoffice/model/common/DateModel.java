package br.com.equiplano.nova.asgard.backoffice.model.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDateTime;

import static br.com.equiplano.nova.asgard.backoffice.util.DatePatterns.*;
import static br.com.equiplano.nova.asgard.backoffice.util.Dates.*;

@Getter
@Setter
@Embeddable
public class DateModel implements Serializable {

    private static final long serialVersionUID = 5296071796628634422L;

    public static final String CREATED_AT = "createdAt";

    @Column(nullable = false)
    public LocalDateTime createdAt;

    @Column(nullable = false)
    public LocalDateTime updatedAt;

    public DateModel() {
    }

    public DateModel(LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public DateModel(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String formattedCreatedAt() {
        return getFormattedDate(createdAt, YYY_MM_DD_T_HH_MM_SS);
    }

    public String formattedUpdatedAt() {
        return getFormattedDate(createdAt, YYY_MM_DD_T_HH_MM_SS);
    }
}
